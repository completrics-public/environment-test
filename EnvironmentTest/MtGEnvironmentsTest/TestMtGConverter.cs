using MtGEnvironments;
using NUnit.Framework;

namespace MtGEnvironmentsTest
{
    public class TestMtGConverter
    {
        [Test]
        public void T1_MtGConverterCalculatesColorlessMana()
        {
            // Given
            var mana = "2";         // Card is, for example: "Mirror Shield" https://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=476485
            var expected = 2;

            // When
            var actual = new MtGConverter().CalculateConvertedManaCost(mana);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void T2_MtGConverterCalculatesNonColorlessMana()
        {
            // Given
            var mana = "BBB";      // Card is, for example: "Ayara, First of Locthwain" ( https://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=473037 )
            var expected = 3;

            // When
            var actual = new MtGConverter().CalculateConvertedManaCost(mana);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void T3_MtGConverterCalculatesAllMana()
        {
            // AS YOU PROBABLY SEE THIS TEST DOES NOT WORK

            // Given
            var mana = "2BBWW";     // Card is, for example: "Dream Trawler" ( https://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=476465 )
            var expected = 6;

            // When
            var actual = new MtGConverter().CalculateConvertedManaCost(mana);

            // Then
            Assert.AreEqual(expected, actual);
        }

    }
}