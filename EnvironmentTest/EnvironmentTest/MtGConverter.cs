﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MtGEnvironments
{
    public class MtGConverter
    {
        /// <summary>
        /// A method taking the manaCost string and returning how expensive this card really is to cast counting all mana.
        /// </summary>
        /// <param name="manaCost">
        /// All possible symbols: W (White), B (Black), U (blUe), G (Green), R (Red)
        /// All possible numbers: 0..15
        /// We have a guarantee that numbers come before symbols; ie:
        ///      WWW  -> 3 White mana, valid record (result: 3)
        ///      2WB  -> 2 Colorless, 1 White, 1 Black mana, valid record (result: 4)
        ///      4    -> 4 Colorless mana, valid record (result: 4)
        ///      W2W  -> INVALID record; numbers come before letters
        /// </param>
        /// <returns></returns>
        public int CalculateConvertedManaCost(string manaCost)
        {
            // This code below does not work. It can cope with colorless mana only ("4", "2") or colored mana
            // ("BBB", "BWU"), but cannot work with both colorless and colored ("2B"). This is what you have
            // to solve. Rewrite the code below so that all three tests are passing.


            var colorlessManaCost = 0;

            try
            {
                colorlessManaCost = int.Parse(manaCost);
            } 
            catch (FormatException)
            {
                // well, means it is NOT a number. So we count all letters.
                colorlessManaCost = manaCost.Length;
            }

            return colorlessManaCost;
        }

    }
}
